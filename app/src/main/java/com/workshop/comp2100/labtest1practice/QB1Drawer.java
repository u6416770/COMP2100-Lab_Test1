package com.workshop.comp2100.labtest1practice;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class QB1Drawer extends View implements View.OnTouchListener
{

    List<Point> points = new ArrayList<>();
    Point p;


    public QB1Drawer(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        this.setOnTouchListener(this);
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        Paint bluePaint = new Paint();
        bluePaint.setColor(Color.BLUE);
        for(Point p : points)
        {
            canvas.drawCircle(p.x,p.y,10.0f,bluePaint);
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent)
    {
        if(motionEvent.getAction() == MotionEvent.ACTION_DOWN)
        {
            p = new Point();
            p.x =  (int) motionEvent.getX();
            p.y =  (int) motionEvent.getY();
            points.add(p);
        }
        this.invalidate();
        return true;
    }
}
