package com.workshop.comp2100.labtest1practice;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QB3Drawer extends View
{

    public QB3Drawer(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        Random random =  new Random();
        Paint paint = new Paint();
        paint.setColor(Color.BLUE);
        paint.setStrokeWidth(10.0f);
        for(int i = 0; i < 50; i++)
        {
            int randomX = random.nextInt(canvas.getWidth());
            int randomY = random.nextInt(canvas.getHeight());
            int randomX2 = random.nextInt(canvas.getWidth());
            int randomY2 = random.nextInt(canvas.getHeight());
            canvas.drawLine(randomX, randomY, randomX2, randomY2, paint);
        }
    }
}
