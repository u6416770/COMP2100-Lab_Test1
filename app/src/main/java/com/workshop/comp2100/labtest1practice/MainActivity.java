package com.workshop.comp2100.labtest1practice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity
{

    private Button qb1;
    private Button qb2;
    private Button qb3;
    private Button qb4;
    private Button qb5;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        qb1 = findViewById(R.id.qb1);
        qb2 = findViewById(R.id.qb2);
        qb3 = findViewById(R.id.qb3);
        qb4 = findViewById(R.id.qb4);
        qb5 = findViewById(R.id.qb5);

        qb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent qb1Switch = new Intent(MainActivity.this, QB1.class);
                startActivity(qb1Switch);
            }
        });

        qb2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent qb2Switch = new Intent(MainActivity.this, QB2.class);
                startActivity(qb2Switch);
            }
        });

        qb3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent qb3Switch = new Intent(MainActivity.this, QB3.class);
                startActivity(qb3Switch);
            }
        });

        qb4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent qb4Switcher = new Intent(MainActivity.this, QB4.class);
                startActivity(qb4Switcher);
            }
        });

        qb5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent qb5Switcher = new Intent(MainActivity.this, QB5.class);
                startActivity(qb5Switcher);
            }
        });
    }
}
