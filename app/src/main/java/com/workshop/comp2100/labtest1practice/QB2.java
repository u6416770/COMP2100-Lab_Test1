package com.workshop.comp2100.labtest1practice;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.widget.EditText;
import android.widget.TextView;

public class QB2 extends AppCompatActivity
{
    private TextView timerText;
    private CountDownTimer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qb2);
        timerText =  findViewById(R.id.timerText);
        timer = new CountDownTimer(10000, 1000)
        {
            @Override
            public void onTick(long l)
            {
                timerText.setText(""+l/1000);
            }

            @Override
            public void onFinish()
            {
                timerText.setText("0");
            }
        }.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        timer.start();
        return super.onTouchEvent(event);
    }
}
