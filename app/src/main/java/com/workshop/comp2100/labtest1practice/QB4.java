package com.workshop.comp2100.labtest1practice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class QB4 extends AppCompatActivity
{

    private Button enterButton;
    private EditText nameInput;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qb4);
        enterButton = findViewById(R.id.enterButton);
        nameInput = findViewById(R.id.nameInput);

        enterButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent greetingSwitcher =  new Intent(QB4.this, QB4_2.class);
                greetingSwitcher.putExtra("Name", nameInput.getText().toString());
                startActivity(greetingSwitcher);
            }
        });

    }

}
