package com.workshop.comp2100.labtest1practice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class QB4_2 extends AppCompatActivity
{
    private TextView greetingText;
    private EditText nameInput;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qb4_2);
        nameInput = findViewById(R.id.nameInput);
        greetingText = findViewById(R.id.greetingText);
        Intent getString = getIntent();
        String name = getString.getStringExtra("Name");
        greetingText.setText(name);
    }
}
